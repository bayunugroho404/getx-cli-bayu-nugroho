import 'package:getx_byu/app/data/network/response/response_get_user.dart';
import 'package:getx_byu/app/utils/constant.dart';
import 'package:http/http.dart' show Client, Response;
import 'dart:convert';

/**
 * Created by Bayu Nugroho
 * Copyright (c) 2021 . All rights reserved.
 */

class APIService {
  Client client = Client();
  String base_url = BASE_URL;

  Future<ResponsegetUser> getUser() async {
    Response response;
    response = await client.get(Uri.parse("$base_url/?result=10"));
    print(response.body);
    if (response.statusCode == 200) {
      return ResponsegetUser.fromJson(json.decode(response.body));
    } else {
      print(response.body);
      throw Exception('Gagal');
    }
  }
}
