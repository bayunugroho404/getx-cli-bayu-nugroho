import 'package:get/get.dart';
import 'package:getx_byu/app/data/network/api_services.dart';
import 'package:getx_byu/app/data/network/response/response_get_user.dart';

class HomeController extends GetxController {
  //TODO: Implement HomeController
  final count = 0.obs;
  var listUser = List<Result>.empty(growable: true).obs;

  @override
  void onInit() {
    super.onInit();
    getData();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  void increment() => count.value++;

  void getData(){
    listUser.clear();
    APIService().getUser().then((data){
      listUser.value = data.results;
      update();
    }).catchError((onError){
      print('$onError');
    });
  }
}
