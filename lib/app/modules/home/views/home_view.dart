import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:getx_byu/app/modules/login/views/login_view.dart';

import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('HomeView'),
        centerTitle: true,
      ),
      body: Obx((){
        if(controller.listUser.length > 0){
          return Text('ada data');
        }else{
          return Center(
            child:CircularProgressIndicator()
          );
        }
      }),
    );
  }
}
